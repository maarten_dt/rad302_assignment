To John,

Tried to get a few things working such as the google map API, but the search function does not work properly.
The project itself runs without any issues on my end, but please let me know if they do on your end.
Git has the latest version and will remain untouched until it has been received by yourself.
Please note that the commits and pushes are close to each other, as opposed to spread out over the semester. This was due to personal time constraints.
If you feel this is suspicious to you, feel free to interview me about all commits, pushes and all of the code written as I am fit to explain all of it.

Finally, I could not get my own API going as I was totally unsure how to go about this in relation to my assignment topic.
I would be more than happy to take personal tips about what kind of APIs I could have done for this assignment, as I really was not sure.

Kind regards,

Maarten D. Tiemessen
S00132821
Software Y3