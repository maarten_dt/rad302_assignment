﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using S00132821_API_V1.Models;

namespace S00132821_API_V1.Controllers
{
    public class GoogleMapController : Controller
    {
        // GET: GoogleMap
        public ActionResult Index()
        {
            CompanyDB CDB = new CompanyDB();
            return View(CDB.Companies.ToList());
        }

        public ActionResult About()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Search(string Company)
        {
            CompanyDB GE = new CompanyDB();
            var result = GE.Companies.Where(x => x.CompanyName.StartsWith(Company)).ToList(); //selecting a company will direct you to the
            //location on google maps
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}