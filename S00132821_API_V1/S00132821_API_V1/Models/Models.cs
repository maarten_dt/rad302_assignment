﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace S00132821_API_V1.Models
{
    public class Companies
    {
        //Company details for database
        [Key]
        public int CompanyID { get; set; }

        [Display(Name = "Company Name"), Required]
        public string CompanyName { get; set; }

        [DisplayName("Location Name"), Required]
        public string LocationName { get; set; }

        //[DataType(DataType.Date)] //Sets datatype of column to datetype
        [DisplayFormat(DataFormatString = "{0:dd-mm-yyyy}", ApplyFormatInEditMode = true)] //Sets format in 0 to day month and year
        [DisplayName("Date founded"), Required]
        public DateTime DateFounded { get; set; }

        //For google maps
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }

    public class CompanyDB : DbContext
	{
        public DbSet<Companies> Companies { get; set; }
        public CompanyDB() : base ("CompanyDB_API") { }
	}
}