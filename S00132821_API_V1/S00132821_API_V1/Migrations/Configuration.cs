namespace S00132821_API_V1.Migrations
{
    using S00132821_API_V1.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<S00132821_API_V1.Models.CompanyDB>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(S00132821_API_V1.Models.CompanyDB context)
        {
            var companylisting = new List<Companies>()
            {
                new Companies() { CompanyName = "InstaLok Dublin", DateFounded = DateTime.Parse("01/01/2014"), LocationName = "Dublin",
                Latitude = 53.339437, Longitude = -6.261329},
                new Companies() { CompanyName = "InstaLok London", DateFounded = DateTime.Parse("05/06/2013"), LocationName = "London",
                Latitude = 51.513661, Longitude = -0.112477},
                new Companies() { CompanyName = "InstaLok Belfast", DateFounded = DateTime.Parse("02/01/2015"), LocationName = "Belfast",
                Latitude = 54.597478, Longitude = -5.933445}
            };
            companylisting.ForEach(Companies => context.Companies.Add(Companies)); //Lambda expr.
            context.SaveChanges();
        }
    }
}
